import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'

import studioStudentList from './views/studio/studioStudentList.vue'
import studioDetail from './views/studio/studioDetail.vue'
import provinces from './views/comCheck/provinces.vue'
import exampaper from './views/exam/exampaper.vue'
import marking from './views/marking/selectSubject'
import paperManage from './views/marking/paperManage'
import markingArea from './views/marking/markingArea'
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: '/',
        component: Home,
        name: '考生管理',
        iconCls: 'el-icon-message',//图标样式class
        iconUrl: '/static/assets/icon/student.png',//图标样式class
        children: [
            { path: '/studioTable', component:()=>import('./views/studio/studioTable.vue') , name: '画室管理' },
            { path: '/studioDetail', component: studioDetail, name: '画室生源查询',hidden: true},
            { path: '/studioStudentList', component: studioStudentList, name: '画室学生查询',hidden: true},
            { path: '/examRoom', component:()=>import('./views/studio/examRoom.vue') , name: '考场管理' },
            { path: '/examApprove', component:()=>import('./views/student/examApprove.vue'), name: '准考证管理' },
        ]
    },{
        path: '/',
        component: Home,
        name: '试卷管理',
        iconCls: 'fa fa-id-card-o',
        iconUrl: '/static/assets/icon/exampaper.png',//图标样式class
        roles:['教辅老师','阅卷老师'],
        children: [
            { path: '/exampaper', component: exampaper, name: '上传试卷' ,roles:['教辅老师'] },
            { path: '/examPaperAssign', component:()=>import('./views/exam/examPaperAssign.vue'), name: '试卷分组' },
            { path: '/examPaperAssignDetail', component:()=>import('./views/exam/examPaperAssignDetail.vue'), name: '查看试卷分组',hidden: true },
            { path: '/selectSubject', component:()=>import('./views/marking/selectSubject.vue'), name: '评卷台',roles:['阅卷老师'],delAdmin:true},
            // { path: '/markingPage', component:()=>import('./views/marking/markingArea.vue'), name: '评分台',hidden: true},
            // { path: '/paperManage', component:()=>import('./views/marking/paperManage.vue'), name: '评分管理',hidden: true},
        ]
    },{
        path: '/',
        component: Home,
        name: '成绩管理',
        iconCls: 'fa fa-address-card',
        iconUrl: '/static/assets/icon/grade.png',//图标样式class
        // leaf: true,//只有一个节点
        children: [
            { path: '/scoreList', component: ()=>import('./views/score/scoreList.vue'), name: '成绩查询',},
            { path: '/scoreExport.', component: ()=>import('./views/score/scoreExport.vue'), name: '试卷导出' ,}
        ]
    },{
        path: '/',
        component: Home,
        name: '系统设置',
        iconCls: 'fa fa-bar-chart',
        iconUrl: '/static/assets/icon/system.png',//图标样式class
        children: [
            { path: '/gradeRule', component: ()=>import('./views/manager/gradeRule.vue'), name: '规则管理' },
            { path: '/users', component:()=>import('./views/manager/users.vue'), name: '权限设置' }
        ]
    },{
        path: '/',
        component: Home,
        name: '测试页',
        iconCls: 'fa fa-bar-chart',
        iconUrl: '/static/assets/icon/system.png',//图标样式class
        hidden: true,
        children: [
            {
                path: '/marking',
                component: marking,
                name: '阅卷系统',
            },
            { path: '/paperManage/:province/:subject', component: paperManage, name: '试卷管理'},
            { path: '/markingArea/:province/:subject/:level/:start', component: markingArea, name: '评卷台'},
            { path: '/provinces', component: provinces, name: '多选框测试' },
            { path: '/examceil', component: ()=>import('./views/studio/examceil.vue'), name: '考试分配测试' },
            { path: '/test2', component: ()=>import('./views/studio/test2.vue'), name: '插件测试' },
        ]
    },{
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;