const gradeCode = [
  {id: '110000',grade: 'A'},
  {id: '120000',grade: 'B'},
  {id: '130000',grade: 'C'},
  {id: '140000',grade: 'D'},
  {id: '150000',grade: 'E'},
  {id: '210000',grade: 'F'},
  {id: '220000',grade: 'G'},
  {id: '230000',grade: 'H'},
  {id: '310000',grade: 'I'},
  {id: '320000',grade: 'J'},
  {id: '330000',grade: 'K'},
  {id: '340000',grade: 'L'},
  {id: '340000',grade: 'M'},
  {id: '340000',grade: 'N'},
  {id: '340000',grade: 'O'},
]

export default gradeCode