//封装的request.js文件
import axios from 'axios';

const baseURL = 'http://47.97.72.64:8080/art-exam-manager'
// const baseURL = '/art-exam-manager'

export function basicRequest(config) {
    const instance = axios.create({
        baseURL: baseURL,
        timeout: 5000,
    });

    instance.interceptors.response.use(
        (res) => {
            return res.data.result;
        },
        (err) => console.log(err)
    );

    instance.interceptors.request.use(
        (config) => {
            config.headers['Art-Token'] = window.sessionStorage.getItem(
                'Art-Token'
            );
            config.headers.Authorization = window.sessionStorage.getItem(
                'Art-Token'
            );
            return config;
        },
        (err) => console.log(err)
    );

    return instance(config);
}

