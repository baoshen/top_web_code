import axios from 'axios';

let base = '';
export const requestLogin = params => { return axios.post(`${base}/login`, params).then(res => res.data); };
export const getUserList = params => { return axios.get(`${base}/user/list`, { params: params }); };
export const getUserListPage = params => { return axios.get(`${base}/user/listpage`, { params: params }); };
export const removeUser = params => { return axios.get(`${base}/user/remove`, { params: params }); };
export const batchRemoveUser = params => { return axios.get(`${base}/user/batchremove`, { params: params }); };
export const editUser = params => { return axios.get(`${base}/user/edit`, { params: params }); };
export const addUser = params => { return axios.get(`${base}/user/add`, { params: params }); };
export const getRole=params=>{return axios.get(`${base}/user/add`, { params: params }); };
export const getStudioListPage=params=>{return axios.get(`${base}/studio/listpage`, { params: params }); };
export const getStudenList=params=>{return axios.get(`${base}/studio/studentlist`, { params: params }); };
export const getStudioDetail=params=>{return axios.get(`${base}/studio/detail`, { params: params }); };
export const exampaperlist=params=>{return axios.get(`${base}/exam/paperlist`, { params: params }); };
export const exampostpaper=params=>{return axios.post(`${base}/exam/postpaper`, { params: params }); };
let head={headers: {'Content-Type': 'application/json'}}
let baseurl='http://47.97.72.64:8080/art-exam-manager/'
// let baseurl='http://192.168.6.227:8080/art-exam-manager/'

export const login_user= params => { return axios.post(`${baseurl}/user/login`, params,{headers: {'Content-Type': 'application/json'}}).then(res => res.data); };
export const user_info= params => { return axios.get(`${baseurl}/user/info`, params,{headers: {'Content-Type': 'application/json'}}).then(res => res.data); };
export const user_info_page= params => { return axios.post(`${baseurl}/userinfo/page`, params,{headers: {'Content-Type': 'application/json'}}).then(res => res.data); };
export const user_info_create= params => {
  return axios.post(`${baseurl}/userinfo/create`, params,head).then(res => res.data);
};
export const user_info_update= params => {
  return axios.post(`${baseurl}/userinfo/update`, params,head).then(res => res.data);
};
export const paper_page= params => {
  return axios.post(`${baseurl}/paper/page`, params,{headers: {'Content-Type': 'application/json'}}).then(res => res.data);
};

export const studio_listStudio= params => {
  return axios.post(`${baseurl}/studio/listStudio`, params,head).then(res => res.data);
};
export const examinee_listByStudioCode= params => {
  return axios.post(`${baseurl}/examinee/listByStudioCode`, params,head).then(res => res.data);
};
export const studio_operation= params => {
  return axios.post(`${baseurl}/studio/operationActivity`, params,head).then(res => res.data);
};
export const score_queryScore= params => {
  return axios.post(`${baseurl}/score/queryScore`, params,head).then(res => res.data);
};
export const examinee_listExamineeInfo= params => {
  return axios.post(`${baseurl}/examinee/listExamineeInfo`, params,head).then(res => res.data);
};



export const getStudioList= () => {
  return axios.post(`${baseurl}/studio/listStudio`, {size:1000,current:1},head).then(res => {
    return new Promise((resolve,reject)=>{
      resolve(res.data.result.records)
    })

  });
};



let ossurl=' http://47.97.72.64:8080/art-exam-manager/aliyunOss/assumeRole'
let headercorss={headers: {'Access-Control-Allow-Origin': '*'}}
export const getOssToken= params => { return axios.get(`${ossurl}`, params).then(res => res.data); };

//规则管理
let gradeRuleurl='http://47.97.72.64:8080/art-exam-manager/ruler/query?province=浙江'
export const getgradeRule= params => { return axios.get(`${gradeRuleurl}`,params).then(res => res.data); };

export const gradeRule_create= params => {
  return axios.post(`${baseurl}/ruler/create`, params,head).then(res => res.data);
};


export const gradeRule_update= params => {
  return axios.post(`${baseurl}/ruler/update`, params,head).then(res => res.data);
};