import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'

Vue.use(Vuex)

// 应用初始状态
const state = {
    count: 10,
    studio:{name:'',studentsCount:10},
    exampaper:[
        {key:'color',total:2000,exist:300,text:'素描',color:'#484'},
        {key:'writing',total:3000,exist:1300,text:'色彩',color:'#559'},
        {key:'sketch',total:2000,exist:1300,text:'速写',color:'#a66'},
        {key:'compose',total:1000,exist:900,text:'设计',color:'pink'}
      ],
    roles:{
        "管理员":'/studioTable',
        "教辅老师":'/exampaper',
        "阅卷老师":'/selectSubject',
    },
    login:{
        name:'',
        role:''
    }
}

// 定义所需的 mutations
const mutations = {
    INCREMENT(state) {
        state.count++
    },
    DECREMENT(state) {
        state.count--
    }
}

// 创建 store 实例
export default new Vuex.Store({
    actions,
    getters,
    state,
    mutations
})