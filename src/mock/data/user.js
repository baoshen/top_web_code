import Mock from 'mockjs';
const LoginUsers = [
  {
    id: 1,
    username: 'admin',
    password: '123456',
    avatar: 'https://raw.githubusercontent.com/taylorchen709/markdown-images/master/vueadmin/user.png',
    name: '张某某',
    role:'管理员'
  },{
    id: 2,
    username: 'help',
    password: '123456',
    avatar: 'https://raw.githubusercontent.com/taylorchen709/markdown-images/master/vueadmin/user.png',
    name: '张某某',
    role:'教辅人员'
  },{
    id: 3,
    username: 'teacher',
    password: '123456',
    avatar: 'https://raw.githubusercontent.com/taylorchen709/markdown-images/master/vueadmin/user.png',
    name: '张某某',
    role:'阅卷老师'
  }
];

Mock.Random.extend({
  phone: function () {
    var phonePrefixs = ['132', '135', '189'] // 自己写前缀哈
    return this.pick(phonePrefixs) + Mock.mock(/\d{8}/) //Number()
  }
})
const Users = [];

for (let i = 0; i < 86; i++) {
  Users.push(Mock.mock({
    id: Mock.Random.guid(),
    name: Mock.Random.cname(),
    addr: Mock.mock('@county(true)'),
    'age|18-60': 1,
    birth: Mock.Random.date(),
    sex: Mock.Random.integer(0, 1)
  }));
}

const studios=[]
for (let i = 0; i < 186; i++) {
  studios.push(Mock.mock({
    // id: Mock.Random.guid(),
    // id:Mock.mock('@phone'),
    'id|+1':i,
    studioname: '画室'+Mock.Random.integer(1, 5),
    province: Mock.mock('@province'),
    teacher:Mock.Random.cname(),
    'studentsCount|30-300':50,
    phone:Mock.mock('@phone'),
    email: Mock.mock('@email'),
  }));
}
const students=[]
for (let i = 0; i < 186; i++) {
  students.push(Mock.mock({
    id: '202010000'+i,
    studentname:Mock.Random.cname(),
    studioname: '画室'+Mock.Random.integer(1, 5),
    province: Mock.mock('@province(true)'),
    identity:'202010000'+i,
    examroomid:  Mock.Random.integer(0, 30),
  }));
}
const exampapers=[]
for (let i = 0; i < 186; i++) {
  exampapers.push(Mock.mock({
    id: '202010000'+i,
    studentname:Mock.Random.cname(),
    studioname: '画室'+Mock.Random.integer(1, 5),
    province: Mock.mock('@province(true)'),
    identity:'202010000'+i,
    examroomid:  Mock.Random.integer(0, 30),
    color:'',
    writing:'',
    sketch:'',
    compose:'',
  }));
}
export { LoginUsers, Users,studios,students };
